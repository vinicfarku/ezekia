<?php

use Illuminate\Database\Seeder;

use App\Models\Currency;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        Currency::updateOrCreate(
            array('code' => 'EUR'),
            array('name' => 'Euro')
        );

        Currency::updateOrCreate(
            array('code' => 'USD'),
            array('name' => 'US Dollar')
        );
        
        Currency::updateOrCreate(
            array('code' => 'GBP'),
            array('name' => 'Great British Pound')
        );
    }
}
