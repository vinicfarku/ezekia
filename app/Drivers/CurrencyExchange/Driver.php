<?php

namespace App\Drivers\CurrencyExchange;

use App\Contracts\CurrencyExchangeContract;

abstract class Driver implements CurrencyExchangeContract
{
    // Currency 3 letter code to convert $amount from
    protected $from;

    // Currency 3 letter code to convert $amount to
    protected $to;

    // Amount to be converted
    protected $amount;

    /**
     * Convert a specified amount from one currency to another.
     */
    abstract public function convert();

    public function from(string $from) {
        $this->from = $from;
        return $this;
    }

    public function to(string $to) {
        $this->to = $to;
        return $this;
    }

    public function amount(int $amount) {
        $this->amount = $amount;
        return $this;
    }
}
