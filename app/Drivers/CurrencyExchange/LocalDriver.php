<?php

namespace App\Drivers\CurrencyExchange;

use App\Contracts\CurrencyExchangeContract;

class LocalDriver extends Driver
{
    /**
     * Convert a specified amount from one currency to another.
     */
    public function convert() {
        $exchange_rate = config("currency_exchange.rates.$this->from.$this->to", null);

        if($exchange_rate == null) {
            throw (new \Exception("Conversion Rate doesn\'t exist when converting from `$this->from` to `$this->to`"));
        } else if ($this->from === $this->to) {
            return $this->amount;
        } else {
            return $this->amount * $exchange_rate;
        }
    }
}
