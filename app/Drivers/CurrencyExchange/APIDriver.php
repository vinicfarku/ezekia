<?php

namespace App\Drivers\CurrencyExchange;

use App\Contracts\CurrencyExchangeContract;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class APIDriver extends Driver
{
    /**
     * Convert a specified amount from one currency to another.
     */
    public function convert() {
        if ($this->from === $this->to) {
            return $this->amount;
        } else {
            $guzzleClient = new Client;
            $response = $guzzleClient->request('GET', "https://api.exchangeratesapi.io/latest?symbols=$this->to&format=1&base=$this->from");

            if($response->getStatusCode() == 200) {
                $code = $this->to;
                $data = json_decode($response->getBody()->getContents());
                $exchange_rate = $data->rates->$code;
                return $this->amount * $exchange_rate;
            } else {
                return null;
            }
        }
    }
}
