<?php

namespace App\Managers;

use App\Drivers\CurrencyExchange\APIDriver;
use App\Drivers\CurrencyExchange\LocalDriver;

use Illuminate\Support\Manager;

class CurrencyExchangeManager extends Manager
{
    public function getDefaultDriver()
    {
        return $this->config->get('currency_exchange.driver');
    }

    public function createAPIDriver()
    {
        return new APIDriver();
    }

    public function createLocalDriver()
    {
        return new LocalDriver();
    }
}
