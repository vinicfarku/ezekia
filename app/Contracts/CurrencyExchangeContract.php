<?php

namespace App\Contracts;

interface CurrencyExchangeContract
{
    /**
     * Convert a specified amount from one currency to another.
     */
    public function convert();
}
