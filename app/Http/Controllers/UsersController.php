<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\SaveUser;

use App\Models\Currency;
use App\Models\User;

use CurrencyExchanger;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(10);
        return view('frontend.users.index')->with(compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $currencies = Currency::all();
        return view('frontend.users.save')->with(compact('currencies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaveUser $request)
    {
        $user = new User;
        $user->name         = $request->get('name');
        $user->email        = $request->get('email');
        $user->rate         = $request->get('rate');
        $user->currency_id  = $request->get('currency_id');
        $user->password     = '';
        $user->save();

        return redirect()->route('users.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $currencies = Currency::all();
        return view('frontend.users.show')->with(compact('user', 'currencies'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function exchange(Request $request, User $user) {
        $currency = Currency::findOrFail($request->currency_id);
        $new_amount = CurrencyExchanger::from($user->currency->code)->to($currency->code)->amount($user->rate)->convert();

        return response()->json(array('data' => array('new_amount' => $new_amount, 'new_currency' => $currency->code)));
    }
}
