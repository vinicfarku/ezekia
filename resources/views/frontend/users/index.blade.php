@extends('layouts.frontend')

@section('content')
    <section class="container mt-4">
        <div class="row mb-4">
            <div class="offset-md-9 col-md-3 col-12">
                <a href="{{ route('users.create') }}" class="btn btn-block btn-primary">Register Freelancer</a>
            </div>
        </div>
        @forelse ($users as $user)
            <div class="card col-12 mb-4">
                <div class="card-body">
                    <h5 class="card-title">{{ $user->name }}</h5>
                <p class="card-text">Click View User Details to view more information regarding the freelancer.</p><p>
                     <strong>{{ $user->rate . " " . $user->currency->code . " per hour" }}. </strong></p>
                    <a href="{{ route('users.show', compact('user')) }}" class="btn btn-primary">View user details</a>
                </div>
            </div>
        @empty
            <div class="card col-12">
                <div class="card-body">
                    <p class="card-text">No freelancers are currently registered. Please register a new one</p>
                </div>
            </div>
        @endforelse
        {{ $users->links() }}
    </section>
@stop
