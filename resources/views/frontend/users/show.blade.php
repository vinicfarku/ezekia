@extends('layouts.frontend')

@push('styles')
    <style>
        .required:after {
            content: " *";
            color: red;
        }
    </style>
@endpush

@section('content')
    <section class="container mt-4">
        <div class="card col-12 p-4">
            <h5 class="card-title">{{ $user->name }}</h5>
            <p class="card-text text-muted">{{ $user->email }}</p>
            <p class="card-text">{{ $user->rate . " " . $user->currency->code . " per hour" }}</p>
        </div>
        <div class="card col-12 p-4 mt-4">
            <form method="POST" action="{{ route('users.exchange', compact('user')) }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-12">
                        <label for="currency_id" class="required">Exchange To</label>
                        <select name="currency_id" id="currency_id" class="form-control">
                            <option value="">Please choose the currency in which you would prefer to be paid</option>
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="md-offset-9 col-md-3 col-12 float-right mt-4">
                        <button type="submit" class="btn btn-block btn-primary">Exchange</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@stop
