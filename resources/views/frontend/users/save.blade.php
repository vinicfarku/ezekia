@extends('layouts.frontend')

@push('styles')
    <style>
        .required:after {
            content: " *";
            color: red;
        }
    </style>
@endpush

@section('content')
    <section class="container mt-4">
        <div class="card col-12 p-4">
            <form method="POST" action="{{ route('users.store') }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-12 col-md-6">
                        <label for="name" class="required">Name</label>
                        <input type="text" name="name" id="name" class="form-control">
                        @if($errors->has('name'))
                            <p class="text-danger">{{ $errors->first('name') }}</p>
                        @endif
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="email" class="required">Email</label>
                        <input type="text" name="email" id="email" class="form-control">
                        @if($errors->has('email'))
                            <p class="text-danger">{{ $errors->first('email') }}</p>
                        @endif
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="rate" class="required">Hourly Rate</label>
                        <input type="text" name="rate" id="rate" class="form-control">
                        @if($errors->has('rate'))
                            <p class="text-danger">{{ $errors->first('rate') }}</p>
                        @endif
                    </div>
                    <div class="col-12 col-md-6">
                        <label for="currency_id" class="required">Currency</label>
                        <select name="currency_id" id="currency_id" class="form-control">
                            <option value="">Please choose the currency in which you would prefer to be paid</option>
                            @foreach($currencies as $currency)
                                <option value="{{ $currency->id }}">{{ $currency->name }}</option>
                            @endforeach
                        </select>
                        @if($errors->has('currency_id'))
                            <p class="text-danger">{{ $errors->first('currency_id') }}</p>
                        @endif
                    </div>
                    <div class="col-md-3 offset-md-6 col-12 float-right mt-4">
                        <a href="{{ route('users.index') }}" class="btn btn-block btn-danger">Cancel</a>
                    </div>
                    <div class="col-md-3 col-12 float-right mt-4">
                        <button type="submit" class="btn btn-block btn-primary">Register</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@stop
