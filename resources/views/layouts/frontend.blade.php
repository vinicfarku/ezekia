@extends('layouts.core')

@section('main')
    @include('partials.frontend.header')

    @yield('content')

    @include('partials.frontend.footer')
@stop
