<?php

return array(
    /*
     *  Can be one among 'Local' and 'API'
     *  Choosing 'Local' makes use of the 'rates' arrays found in this file further below
     *  Choosing 'API' makes use of the https://exchangeratesapi.io/ api to determine the converted value
     */
    'driver'    => env('CURRENCY_EXCHANGE_DRIVER', 'Local'),

    'rates' => array(
        'EUR' => array(
            'USD' => 1.19,
            'GBP' => 0.89
        ),
        'USD' => array(
            'EUR' => 0.84,
            'GBP' => 0.75
        ),
        'GBP' => array(
            'USD' => 1.33,
            'EUR' => 1.12
        ),
    )
);
