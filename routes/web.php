<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'users');

Route::prefix('users')->name('users.')->group(function() {
    Route::get('', 'UsersController@index')->name('index');
    Route::get('create', 'UsersController@create')->name('create');
    Route::post('store', 'UsersController@store')->name('store');
    Route::get('{user}', 'UsersController@show')->name('show');
    Route::post('{user}/exchange', 'UsersController@exchange')->name('exchange');
});
